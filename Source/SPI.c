/*
 * File: SPI.c
 *
 * Created: 21.05.2021 12:43:40
 * Author : Sebastian Giessauf
 */ 

#include "SPI.h"

const unsigned char SPIPrescalerValues[] = {4, 8, 64, 128};

// Public Function Declaration
TBool spiInit(long cpuClock, int speed);
TBool spiSendReceive(char* sendBuffer, char* receiveBuffer, int size);

// Private Function Declaration
TBool spiSendReceiveByte(char send, char* receive);

// Implementation of Functions

/* Function: spiInit
 * Initializes the SPI Interface
 * 
 * Parameter:
 *  long cpuClock - Clockrate of the CPU
 *  int bitrate - Bitrate of the SPI interface
 */
TBool spiInit(long cpuClock, int bitrate) {
    // Initialise SPI Pins
    DDRB |= (1 << DDB5) | (1 << DDB6) | (1 << DDB7);
    
    // Enable SPI and set prescaler
    char spr = 0;
    char spi2x = 0;
    for (int i = 0; i < sizeof(SPIPrescalerValues); i ++) {
        if (!(cpuClock / (SPIPrescalerValues[i] / 2) > bitrate)) {
            SPCR |= (1 << SPE) | SPIPrescalerValues[i];
            SPSR |= 1;
            break;
        }
        if (!(cpuClock / (SPIPrescalerValues[i]) > bitrate)) {
            SPCR |= (1 << SPE) | SPIPrescalerValues[i];
            SPSR &= 0;
            break;
        }        
    }
    
    
    return ETRUE;
}

/* Function: spiSendReceive
 * Sends and receives a certain amount of data
 * 
 * Parameters:
 *  char* sendBuffer - Data to send
 *  char* receiveBuffer - Buffer for received Data
 *  int size - Size of both buffers
 */
TBool spiSendReceive(char* sendBuffer, char* receiveBuffer, int size) {
    for (int i = 0; i < size; i ++)
        if (spiSendReceiveByte(sendBuffer[i], &receiveBuffer[i]))
            return EFALSE;
    
    return ETRUE;
}

/* Function: spiSendReceiveByte
 * Sends and receives 1 Byte of Data
 *
 * Parameters:
 *  char send - the byte to send
 *  char receive - the received byte
 */
TBool spiSendReceiveByte(char send, char* receive) {
    SPDR = send;
    // Wait for cycle
    while(!(SPSR & (1<<SPIF)));
    *receive = SPDR;
    return ETRUE;
}