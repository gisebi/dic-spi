/*
 * File: SPI.h
 *
 * Created: 21.05.2021 12:43:40
 * Author : Sebastian Giessauf
 */ 

#ifndef SPI_H
#define SPI_H

#include "HtlStddef.h"
#include "avr\iom644p.h"
#include <avr/io.h>


/* Function: spiInit
 * Initializes the SPI Interface
 * 
 * Parameter:
 *  long cpuClock - Clockrate of the CPU
 *  int bitrate - Bitrate of the SPI interface
 */
TBool spiInit(long cpuClock, int bitrate);

/* Function: spiSendReceive
 * Sends and receives a certain amount of data
 * 
 * Parameters:
 *  char* sendBuffer - Data to send
 *  char* receiveBuffer - Buffer for received Data
 *  int size - Size of both buffers
 */
TBool spiSendReceive(char* sendBuffer, char* receiveBuffer, int size);

#endif